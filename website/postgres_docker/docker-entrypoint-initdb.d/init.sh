psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER xwiki;
CREATE DATABASE xwiki;
GRANT ALL PRIVILEGES ON DATABASE xwiki TO xwiki;
ALTER ROLE xwiki WITH PASSWORD 'xwiki';
ALTER USER xwiki WITH SUPERUSER;
EOSQL

