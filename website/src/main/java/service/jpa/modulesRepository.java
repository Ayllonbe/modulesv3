package service.jpa;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import service.model.module;
@Repository
public interface modulesRepository extends JpaRepository<module, Long>{
	module findByName(String name);
	
}
