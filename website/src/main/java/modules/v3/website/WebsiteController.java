package modules.v3.website;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import service.jpa.modulesRepository;
import service.model.module;
import service.tools.Read;

@Controller
public class WebsiteController {

	@Autowired
	private modulesRepository mRepository;

	@RequestMapping(value="/{name}", method = RequestMethod.GET)
	public String getModules(Model m,@PathVariable(value = "name") String module) {
		module t = mRepository.findByName(module);
		File fileJSON = new File("src/main/resources/modules_results/"+t.getName()+".json");
		if(fileJSON.exists()) {
			String jsonData = Read.readFileJSON(fileJSON.getAbsolutePath());

			m.addAttribute("json", jsonData);
			m.addAttribute("name",t.getName());
			return "visual";
			}
		else {
				return "error";
			}
		
	}
//	@RequestMapping("/")
//	public String wellcome(Model m) {
//		return "home";
//	}
}
